package it.unibo.opportunisticnet.gpsstormdemo;

import kafka.producer.Partitioner;
import kafka.utils.VerifiableProperties;

/**
 * Partizionatore che distribuisce i dati nelle partizioni eseguendo il modulo della chiave
 *
 * @author Lorenzo Pellegrini
 */
public class SimplePartitioner implements Partitioner {
	public SimplePartitioner (VerifiableProperties props) {

	}

	@Override
	public int partition(Object key, int a_numPartitions) {
		int keyValue = Integer.parseInt( (String) key );
		return keyValue % a_numPartitions;
	}

}
