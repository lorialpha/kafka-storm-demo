package it.unibo.opportunisticnet.gpsstormdemo;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

/**
 * Definisce una posizione sotto forma di latitudine e longitudine
 * </p>
 * La latitudine � rappresentata da un numero compreso tra -90 e +90, la longitudine tra -180 e +180
 *
 * @author Lorenzo Pellegrini
 */
public final class GpsPosition {

	private final double latitude;
	private final double longitude;

	public GpsPosition(double latitude, double longitude) {
		if( latitude < -90 || latitude > 90 ) {
			throw new IllegalArgumentException( "Latitude must be between -90 and 90 (" + latitude + ")" );
		}

		if( longitude < -180 || longitude > 180 ) {
			throw new IllegalArgumentException( "Longitude must be between -180 and 180 (" + longitude + ")" );
		}

		this.latitude = latitude;
		this.longitude = longitude;
	}

	public GpsPosition( GpsPosition other ) {
		this.latitude = other.latitude;
		this.longitude = other.longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public String asKafkaString() {
		return String.format( Locale.ENGLISH, "%f;%f", latitude, longitude);
	}

	public static GpsPosition fromKafkaString(@NotNull String kafkaString) {
		String[] elements = kafkaString.split( ";" );

		if( elements.length != 2 ) {
			throw new IllegalArgumentException( "Invalid string" );
		}

		try {
			double latitude = Double.parseDouble( elements[0] );
			double longitude = Double.parseDouble( elements[1] );

			return new GpsPosition( latitude, longitude );//lancer� un'eccezione se i dati sono errati
		} catch ( NumberFormatException e ) {
			throw new IllegalArgumentException( "Invalid coordinates", e );
		}
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "GpsPosition{" );
		sb.append( "latitude=" ).append( latitude );
		sb.append( ", longitude=" ).append( longitude );
		sb.append( '}' );
		return sb.toString();
	}
}
