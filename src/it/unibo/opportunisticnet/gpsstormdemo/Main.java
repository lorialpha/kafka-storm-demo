package it.unibo.opportunisticnet.gpsstormdemo;

import com.sun.org.apache.xpath.internal.SourceTree;
import it.unibo.opportunisticnet.gpsstormdemo.fakegpsdevice.FakeGpsDevice;
import it.unibo.opportunisticnet.gpsstormdemo.fakegpsdevice.GpsDevice;
import org.omg.CORBA.DynAnyPackage.Invalid;

import java.util.Random;

/**
 * Entry point della demo. In questa demo vengono inviate a un broker Kafka le coordinate di alcuni device.
 * </p>
 * Kafka riceve questo dato come una stringa composta dall'identificatore del device, la latitudine e la longitudine
 * Attualmente la demo permette la sola creazione di device in una posizione casuale fissata. Le classi usate per interagire con Kafka
 * possono essere utilizzate anche su device reali.
 * </p>
 * Esempio di uso: java -jar nomejar.jar -norandom 0.2;0.2 0.4;0.4 0.2;0.6 0.2;0.8 0.6;0.2 0.9;0.3 10.0;-3.0 10.01;-3.1
 *
 * @author Lorenzo Pellegrini
 */
public class Main {

	private static final String NO_RANDOM_DEVICES_OPTION = "-norandom";
	private static final String EXPLICIT_RANDOM_DEVICES_OPTION = "-random";
	private static final int FAKE_GPS_DEVICES_NUMBER = 10;
	private static final int MAX_MILLIS_DEMO = 7000;
	private static final double MIN_LAT = 0.0;
	private static final double MAX_LAT = 1.0;
	private static final double MIN_LON = 0.0;
	private static final double MAX_LON = 1.0;

	/**
	 * Una lista, anche solo parziale, di ip e porte degli host kafka
	 */
	private static final String brokersList = "127.0.0.1:9092,127.0.0.1:9093,127.0.0.1:9094";

	/**
	 * Il nome della classe usata per serializzare i dati. kafka.serializer.StringEncoder permette la codifica di stringhe
	 */
	private static final String serializerClass = "kafka.serializer.StringEncoder";

	/**
	 * Il nome della classe usata per partizionare i dati sulla base della chiave.
	 * </p>
	 * it.unibo.opportunisticnet.gpsstormdemo.SimplePartitioner prende la chiave intera ed esegue il modulo del numero di partizioni del
	 * topic per distribuire i dati sulle partizioni.
	 */
	private static final String partitionerClass = "it.unibo.opportunisticnet.gpsstormdemo.SimplePartitioner";

	/**
	 * Il nome del topic a cui appendere i dati
	 */
	private static final String sendPositionTopic = "devices_position";

	/**
	 * Entry point. Genera le posizioni casuali dei device ed esegue la simulazione per un tempo limitato
	 *
	 * @param args Una serie di coordinate espresse come latitudine;longitudine che verr� usata per la creazione di device. Di default
	 *                verranno inoltre creati 10 device in posizioni casuali tra 0.0;0.0 e 1.0;1.0. Per evitare che vengano creati �
	 *                possibile usare come primo parametro -norandom oppure � possibile specificare quanti device creare casualmente
	 *                usando (come primi 2 parametri) -random numerodidevicecasuali
	 */
	public static void main(String[] args) {
		boolean noRandom;
		boolean explicitRandomNumber;
		int randomDevicesNumber;
		int commandLineDevicesNumber;
		int totalDevicesNumber;

		if( args.length > 0 && NO_RANDOM_DEVICES_OPTION.equals( args[0] ) ) {
			noRandom = true;
			explicitRandomNumber = false;
			commandLineDevicesNumber = args.length-1;
			randomDevicesNumber = 0;
		} else if( args.length > 0 && EXPLICIT_RANDOM_DEVICES_OPTION.equals( args[0] ) ) {
			if( args.length < 2 ) {
				System.out.println("No enough parameters for " + EXPLICIT_RANDOM_DEVICES_OPTION);
				showUsage();
				return;
			}

			noRandom = false;
			explicitRandomNumber = true;
			commandLineDevicesNumber = args.length-2;
			try {
				randomDevicesNumber = Integer.parseInt( args[1] );

				if( randomDevicesNumber < 0 ) {
					System.out.println("Invalid value for " + EXPLICIT_RANDOM_DEVICES_OPTION );
					showUsage();
					return;
				}
			} catch ( NumberFormatException e ) {
				System.out.println("Invalid value for " + EXPLICIT_RANDOM_DEVICES_OPTION );
				showUsage();
				return;
			}
		} else {
			noRandom = false;
			explicitRandomNumber = false;
			commandLineDevicesNumber = args.length;
			randomDevicesNumber = FAKE_GPS_DEVICES_NUMBER;
		}

		totalDevicesNumber=commandLineDevicesNumber+randomDevicesNumber;

		double[][] commandLineDevices = new double[commandLineDevicesNumber][2];
		int argsIndex = (noRandom) ? 1 : (explicitRandomNumber ? 2 : 0);
		for (int i = 0; argsIndex < args.length; i++, argsIndex++) {
			double latitude, longitude;
			String argument = args[argsIndex];
			String[] coordinatesStr = argument.split( ";" );
			if( coordinatesStr.length != 2 ) {
				System.out.println("Invalid coordinate string " + argument);
				showUsage();
				return;
			}

			try {
				latitude = Double.parseDouble( coordinatesStr[0] );
				longitude = Double.parseDouble( coordinatesStr[1] );
			} catch ( NumberFormatException e ) {
				System.out.println("Invalid latitude/longitude format: " + argument);
				showUsage();
				return;
			}

			if( latitude < -90.0 || latitude > 90.0 ) {
				System.out.println("Invalid latitude: " + argument);
				showUsage();
				return;
			}

			if( longitude < -180.0 || longitude > 180.0 ) {
				System.out.println("Invalid longitude: " + argument);
				showUsage();
				return;
			}

			commandLineDevices[i][0] = latitude;
			commandLineDevices[i][1] = longitude;
		}


		Random randomGenerator = new Random(  );
		double[][] devicesPositions = new double[totalDevicesNumber][2];
		GpsDevice[] devices = new GpsDevice[totalDevicesNumber];
		KafkaConfiguration kafkaConfiguration = new KafkaConfiguration( brokersList, serializerClass, partitionerClass, sendPositionTopic );

		//Creo i device definiti dall'utente
		int i;
		for( i = 0; i < commandLineDevicesNumber; i++) {
			double latitude = devicesPositions[i][0] = commandLineDevices[i][0];
			double longitude = devicesPositions[i][1] = commandLineDevices[i][1];

			System.out.println( "[" + i + "] lat: " + latitude + ", lon: " + longitude );

			devices[i] = new FakeGpsDevice( new GpsPosition( latitude, longitude ), i );
		}

		//Genero le posizioni casuali dei device
		for(; i < totalDevicesNumber; i++) {
			double latitude = MIN_LAT + randomGenerator.nextDouble()*(MAX_LAT-MIN_LAT);
			double longitude = MIN_LON + randomGenerator.nextDouble()*(MAX_LON-MIN_LON);

			devicesPositions[i][0] = latitude;
			devicesPositions[i][1] = longitude;

			System.out.println( "[" + i + "] lat: " + latitude + ", lon: " + longitude );

			devices[i] = new FakeGpsDevice( new GpsPosition( latitude, longitude ), i );
		}

		for (i = 0; i < totalDevicesNumber; i++) {
			int millisTimeout = 1000 + randomGenerator.nextInt( 4000 );
			System.out.println("[" + i + "] Position update every " + millisTimeout + " ms");

			devices[i].startSendingPosition( kafkaConfiguration, millisTimeout );
		}

		try {
			Thread.sleep( MAX_MILLIS_DEMO );
		} catch ( InterruptedException e ) {
			e.printStackTrace();
		}

		for (i = 0; i < totalDevicesNumber; i++) {
			devices[i].endSendingPosition();
		}
	}

	private static void showUsage() {
		System.out.println("The parameters you entered are not valid!\n" +
		                   "You can execute this program:\n- Without any command line arguments\n- With a list of" +
		                   " devices: each device must be defined as latitude;longitude");

	}
}
