package it.unibo.opportunisticnet.gpsstormdemo;

import org.jetbrains.annotations.NotNull;

/**
 * Bundle usato per mantenere i dati di configurazione di Kafka.
 * </p>
 * Viene memorizzata la lista dei broker, il nome della classe del serializzatore, del partizionatore e il nome del topic
 *
 * @author Lorenzo Pellegrini
 */
public final class KafkaConfiguration {

	private final String brokersList;
	private final String serializerClass;
	private final String partitionerClass;
	private final String topic;

	public KafkaConfiguration(@NotNull String brokersList,
	                          @NotNull String serializerClass,
	                          @NotNull String partitionerClass,
	                          @NotNull String topic) {
		this.brokersList = brokersList;
		this.serializerClass = serializerClass;
		this.partitionerClass = partitionerClass;
		this.topic = topic;
	}

	/**
	 * Ritorna una stringa che rappresenta la lista di broker da cui eseguire il bootstrap
	 *
	 * @return Una lista di broker nel formato ip:port, separati da una virgola
	 */
	@NotNull
	public String getBrokersList() {
		return brokersList;
	}

	/**
	 * Il nome completo della classe del partizionatore
	 *
	 * @return Il nome della classe del partizionatore
	 */
	@NotNull
	public String getPartitionerClass() {
		return partitionerClass;
	}

	/**
	 * Il nome completo della classe del serializzatore
	 *
	 * @return Il nome della classe del serializzatore
	 */
	@NotNull
	public String getSerializerClass() {
		return serializerClass;
	}

	/**
	 * Il nome del topic
	 *
	 * @return Il nome del topic
	 */
	public String getTopic() {
		return topic;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "KafkaConfiguration{" );
		sb.append( "brokersList='" ).append( brokersList ).append( '\'' );
		sb.append( ", serializerClass='" ).append( serializerClass ).append( '\'' );
		sb.append( ", partitionerClass='" ).append( partitionerClass ).append( '\'' );
		sb.append( ", topic='" ).append( topic ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
