package it.unibo.opportunisticnet.gpsstormdemo.fakegpsdevice;

import it.unibo.opportunisticnet.gpsstormdemo.KafkaConfiguration;
import org.jetbrains.annotations.NotNull;

/**
 * Definizione di un device che trasmette a intervalli regolari la sua posizione a un broker Kafka
 *
 * @author Lorenzo Pellegrini
 */
public interface GpsDevice {

	/**
	 * Inizia la trasmissione della posizione a Kafka, usando la configurazione data
	 *
	 * @param configuration La configurazione di Kafka da utilizzare
	 * @param sendTimeoutMillis Ogni quanto inviare la posizione
	 */
	void startSendingPosition(@NotNull KafkaConfiguration configuration, int sendTimeoutMillis);

	/**
	 * Termina la trasmissione della posizione
	 */
	void endSendingPosition();
}
