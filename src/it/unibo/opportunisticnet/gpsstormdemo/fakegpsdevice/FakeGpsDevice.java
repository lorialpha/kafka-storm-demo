package it.unibo.opportunisticnet.gpsstormdemo.fakegpsdevice;

import it.unibo.opportunisticnet.gpsstormdemo.GpsPosition;
import it.unibo.opportunisticnet.gpsstormdemo.KafkaConfiguration;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Properties;

/**
 * Simula un device di posizione fissata
 * </p>
 * Contiene tutto il codice usato per comunicare con Kafka
 *
 * @author Lorenzo Pellegrini
 */
public class FakeGpsDevice implements GpsDevice {

	@NotNull
	private final GpsPosition position;
	private final int deviceId;

	private volatile boolean running;
	private volatile boolean stopRunning;

	@Nullable
	private Thread runningThread;

	public FakeGpsDevice(@NotNull GpsPosition position, int deviceId) {
		this.position = position;
		this.deviceId = deviceId;
		this.running = false;
		this.stopRunning = false;
		this.runningThread = null;
	}

	@Override
	public synchronized void startSendingPosition(@NotNull KafkaConfiguration configuration, int sendTimeoutMillis) {
		if( sendTimeoutMillis <= 0 ) {
			throw new IllegalArgumentException(  );
		}

		if( running ) {
			throw new IllegalStateException( "Already running" );
		}

		stopRunning = false;
		running = true;

		runningThread = new ProducerThread(configuration, position, sendTimeoutMillis);
		runningThread.start();
	}

	@Override
	public synchronized void endSendingPosition() {
		stopRunning = true;
		if ( runningThread != null ) {
			runningThread.interrupt();
			runningThread = null;
		}
	}

	private class ProducerThread extends Thread {

		@NotNull private final KafkaConfiguration configuration;
		@NotNull private final GpsPosition position;
		private final int sendTimeout;

		public ProducerThread(@NotNull KafkaConfiguration configuration, @NotNull GpsPosition position, int sendTimeoutMillis) {
			this.configuration = configuration;
			this.position = position;
			this.sendTimeout = sendTimeoutMillis;
		}

		@Override
		public void run() {
			super.run();

			if( stopRunning ) {
				return;
			}

			try {
				Properties props = new Properties();
				props.put( "metadata.broker.list", configuration.getBrokersList() );
				props.put( "serializer.class", configuration.getSerializerClass() );
				props.put( "partitioner.class", configuration.getPartitionerClass() );//"example.producer.SimplePartitioner"
				props.put( "request.required.acks", "1" );

				ProducerConfig config = new ProducerConfig( props );

				Producer<String, String> producer = new Producer<>( config );

				while ( !stopRunning ) {
					KeyedMessage<String, String> data = new KeyedMessage<>( configuration.getTopic(), deviceId+"",
					                                                        deviceId + ";" + position.asKafkaString() );
					producer.send( data );

					if( stopRunning ) {
						break;
					}

					try {
						Thread.sleep( this.sendTimeout );
					} catch ( InterruptedException ignored ) {}
				}

				producer.close();
			} finally {
				running = false;
			}
		}

		@Override
		public String toString() {
			final StringBuilder sb = new StringBuilder( "ProducerThread{" );
			sb.append( "configuration=" ).append( configuration );
			sb.append( ", position=" ).append( position );
			sb.append( ", sendTimeout=" ).append( sendTimeout );
			sb.append( '}' );
			return sb.toString();
		}
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "FakeGpsDevice{" );
		sb.append( "position=" ).append( position );
		sb.append( ", deviceId=" ).append( deviceId );
		sb.append( ", running=" ).append( running );
		sb.append( '}' );
		return sb.toString();
	}
}
